'use strict'

const builder = (req, res, next) => {
    const context = {
        logger: require('@open-age/logger')('permit:context:builder')
    }

    req.context = context
    if (next) {
        return next()
    }
    return null
}

exports.builder = builder
