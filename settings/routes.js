"use strict";

const fs = require("fs");
const request = require("request");
const specs = require("../specs");

const configure = (app, keys,logger) => {
  const log = logger.start("settings:routes:configure");

  app.get("/specs", function (req, res) {
    fs.readFile("./public/specs.html", function (err, data) {
      if (err) {
        return res.json({
          isSuccess: false,
          error: err.toString(),
        });
      }
      res.contentType("text/html");
      res.send(data);
    });
  });

  app.get("/api/specs", function (req, res) {
    res.contentType("application/json");
    res.send(specs.get());
  });



  ///////////////////////////////////////////////////////////////////////////////////////////
  app.get('/', function (req, res) {
    if (keys.apiKey == "" || keys.apiKey == "YOUR_API_KEY" || keys.affId == "" || keys.affId == "YOUR_AFFILIATE_ID") {
      console.error("YOU NEED TO REPLACE THE apiKey or affId on lines 5 and 6 of app.js");
      process.exit(0);
    }

    var opts = {
      title: "Refill Rx",
      url: "",
      token: "",
      affId: keys.affId,
      devinf: keys.devinf,
      appver: keys.appver
    };
    getLandingURL(function (json) {
      if (json.url != "" && json.token != "") {
        opts.url = json.url;
        opts.token = json.token;
        res.render("../views/pages/index", opts);
      } else {
        res.render("../views/pages/error", {
          title: "Error"
        });
      }
    });
  });

  app.get('/callback', function (req, res) {
    var rx = (req.query.rx) ? req.query.rx : "";

    res.render("../views/pages/callback", {
      title: rx,
      rx: rx
    }) 
    
  });

  function getLandingURL(callback) {
    var options = {
      method: 'POST',
      url: 'https://services-qa.walgreens.com/api/util/mweb5url',
      json: true,
      body: {
        apiKey: keys.apiKey,
        affId: keys.affId,
        transaction: "refillByScan",
        act: "mweb5Url",
        view: "mweb5UrlJSON",
        devinf: keys.devinf,
        appver: keys.appver
      }
    };
    request(options, function (err, response, body) {
      if (!err && response.statusCode == 200) {
        console.log({
          status: "success",
          result: body
        });
        callback({
          url: body.landingUrl,
          token: body.token
        });
      } else {
        console.log({
          status: "error",
          result: err
        });
        callback({
          url: "",
          token: ""
        });
      }
    });
  }
  log.end()
  //-------------------- users routes ----------------------------
  // app.post('/api/users/verifyUser', auth.context.builder, api.users.verifyUser);
};
exports.configure = configure;