"use strict";

global.Promise = require("bluebird")

const express = require("express");
const morgan = require("morgan");
const appConfig = require("config").get("app");
const logger = require("@open-age/logger")("server");
const port = process.env.PORT || appConfig.port || 3000;
const Http = require("http");

const app = express();
var server = Http.createServer(app)
  var keys = {
    affId: "rxapi",
    apiKey: "tiu73FBOaWbUQYxG3rUlArovyIuFTK02",
    devinf: "NodeJS,10.12",
    appver: "1.0"
  }
const bodyParser = require("body-parser");
app.use(express.static(__dirname + '/public'));
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(
  bodyParser.json({
    limit: "70mb",
  })
);

app.use(
  bodyParser.urlencoded({
    limit: "70mb",
    extended: true,
    parameterLimit: 70000,
  })
);




const boot = () => {
  const log = logger.start("app:boot");
  log.info(`environment:  ${process.env.NODE_ENV}`);
  log.info("starting server ...");

  server.listen(port, () => {
    console.log(`listening on port: ${port}`);
    log.end();
  });
};

const init = () => {
  require("./settings/database").configure(logger);
  require("./settings/express").configure(app, logger);
  require("./settings/routes").configure(app,keys, logger);
  boot();
};

app.use(morgan("dev"));
init();